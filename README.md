# scorelines.creative-commons-exporter

Scorelines.CreativeCommonsExporter exports data from Scorelines' applications (croquetscores.com, gateballscores.com). The exported data is made available under a Creative Commons license TBD.