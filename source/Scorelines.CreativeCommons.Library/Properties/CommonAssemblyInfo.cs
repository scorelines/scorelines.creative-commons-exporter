﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Scorelines")]
[assembly: AssemblyProduct("Scorelines")]
[assembly: AssemblyCopyright("Copyright © 2015 Tim Murphy")]
[assembly: ComVisible(false)]

// todo: Version info updated by build process

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]