﻿using System.Reflection;

[assembly: AssemblyTitle("Scorelines.CreativeCommons.Exporter")]
// todo: update creative commons license info
[assembly: AssemblyDescription("Scorelines.CreativeCommonsExporter exports data from Scorelines' applications (croquetscores.com, gateballscores.com). The exported data is made available under a Creative Commons license TBD.")]